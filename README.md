# Create A TODO App
## General Application
The user will use this app to 
1. Add TODO items to a list.
2. Edit and Delete items in the list.

By using laravel:
1. Design the database.
2. Write CRUD APIs for the todo model (use soft delete).
3. Do validation on the incoming requests.
4. Add sort by creation date to todo list.
5. Add Search on todos based on title. If the title contains the query entered the result should return it;
6. Add user management and authentication
    - Each user can see the items posted by other users.
    - Each user can only edit/delete items posted by themselves.
7. **(optional)** If you like, you can have a set public/private option
    public todo items can also be edited/deleted by other users.
8. **(optional)** Adding a Role-Permission mechanism for users have extra points.
    (e.g. a user may can add and edit it but another user may have all the permissions to delete todos ,
    also different roles can be set to users (e.g. admin,regular users))
9. Send an email to user after adding a todo item to inform him that the item has been added.
10. add seeder for users to add 10 users.
11. **(optional)** share the postman collection for your apis

## Infrastructure
Push your code to a github/gitlab repository and share it with us.